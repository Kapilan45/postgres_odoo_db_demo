#!/bin/bash
set -e

POSTGRES_USER='odoo'
DB_NAME='odoo_8_demo'

POSTGRES="psql --username ${POSTGRES_USER}"

echo "Creating database: ${DB_NAME}"
#createdb -U ${POSTGRES_USER} ${DB_NAME}
createdb -U 'odoo' 'odoo_8_demo'
createdb -U 'odoo' 'odoo_9_demo'
createdb -U 'odoo' 'odoo_10_demo'
createdb -U 'odoo' 'odoo_11_demo'
createdb -U 'odoo' 'odoo_12_demo'
createdb -U 'odoo' 'odoo_12ent_demo'
createdb -U 'odoo' 'odoo_13_demo'
createdb -U 'odoo' 'odoo_13ent_demo'
createdb -U 'odoo' 'odoo_14_demo'
createdb -U 'odoo' 'odoo_14ent_demo'

echo "Creating database..."
#psql -U ${POSTGRES_USER} ${DB_NAME} < /${DB_NAME}.sql
psql -U 'odoo' 'odoo_8_demo' < /odoo_8_demo.sql
psql -U 'odoo' 'odoo_9_demo' < /odoo_9_demo.sql
psql -U 'odoo' 'odoo_10_demo' < /odoo_10_demo.sql
psql -U 'odoo' 'odoo_11_demo' < /odoo_11_demo.sql
psql -U 'odoo' 'odoo_12_demo' < /odoo_12_demo.sql
psql -U 'odoo' 'odoo_12ent_demo' < /odoo_12ent_demo.sql
psql -U 'odoo' 'odoo_13ent_demo' < /odoo_13_demo.sql
psql -U 'odoo' 'odoo_13_demo' < /odoo_13ent_demo.sql
psql -U 'odoo' 'odoo_14_demo' < /odoo_14_demo.sql
psql -U 'odoo' 'odoo_14ent_demo' < /odoo_14ent_demo.sql

echo "All database are created."

exit
