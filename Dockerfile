FROM postgres:9

LABEL maintainer="Kapilan M"

# ENVIRONMENT VARIABLES
ENV POSTGRES_USER odoo
ENV POSTGRES_PASSWORD odoo
ENV POSTGRES_DB postgres

COPY ./initialiser_bdd.sh /docker-entrypoint-initdb.d/initialiser_bdd.sh
COPY ./bdd_sql/* /
# CMD chown -R postgres:postgres var/lib/postgresql/data/